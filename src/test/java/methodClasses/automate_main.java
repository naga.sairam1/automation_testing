package methodClasses;


import java.io.IOException;
import java.time.Duration;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import ObjectClasses.objClose;
import ObjectClasses.objGetListElements;
import ObjectClasses.objMainMenu;
import ObjectClasses.objScreenShot;
import ObjectClasses.objSocialLinks;
import ObjectClasses.objWebsite;
import io.github.bonigarcia.wdm.WebDriverManager;

public class automate_main {
	static WebDriver driver;
	
	@BeforeTest
	public void start() throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		WebDriverManager.edgedriver().setup();
		driver = new EdgeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.manage().window().maximize();
		

	}

	// automation on given web site 
	@Test
	public void getWebsite() throws IOException {
		new objWebsite(driver);
		objWebsite.getUrl("url");
	}
	
	@Test
	public void menu() throws IOException {
		// creating an object for object class
		new objMainMenu(driver);
		new objScreenShot(driver);
		// calling the object class methods.
		// give the number of menu elements in menu method
		objMainMenu.menu(5);
		objScreenShot.ScreenShot("FirstScreenShot");
		
		
	}
	
	@Test
	public void socialLinks() throws IOException {
		// creating an object for object classes
		new objSocialLinks(driver);
		new objScreenShot(driver);
		// calling the object class methods.
		// give the number of menu elements in menu method
		objSocialLinks.getSocialLinks(5);
		
		objScreenShot.ScreenShot("SecondScreenShot");
	}
	@Ignore
	@Test
	public void listElements() {
		new objGetListElements(driver);
		
		objGetListElements.listElements();
	}
	
	@AfterTest
	// calling method to close window.
	public void close() {
		new objClose(driver);
		
		objClose.closeDriver();
	}
}
