package methodClasses;

import java.io.IOException;
import java.time.Duration;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;
import support.automate_helper;

public class W3Main {
	
	static WebDriver driver;
	
	//Start
	@BeforeTest
	public void startBrowser() throws IOException {
		WebDriverManager.edgedriver().setup();
		driver = new EdgeDriver();
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(100));
		
		new automate_helper(driver);
		driver.get(automate_helper.property().getProperty("w3_url"));
	}
	
	//handling menus
	@Test (priority = 2)
	public void menus() throws IOException {
		
		for(int i=1;i<5;i++) {
			automate_helper.getElementByLocator("xpath", "menu_item_"+i+"").click();
		}
		
		automate_helper.getElementByLocator("xpath","home_logo").click();
		
	}
	//handling child windows
	@Test(priority = 3)
	public void childWindows() throws IOException {
		String parentWindow = driver.getWindowHandle();
		for(int i = 1; i<4 ; i++) {
				//clicking on 
				automate_helper.getElementByLocator("xpath", "child_Window_xpath_"+i+"").click();
				Set<String> childWindow = driver.getWindowHandles();
				Iterator<String> it = childWindow.iterator();
				while(it.hasNext()) {
					String child = it.next();
					if(!parentWindow.equalsIgnoreCase(child)){
						  driver.switchTo().window(child);
						  System.out.println(driver.getCurrentUrl());
						  driver.close(); 
					  }
				}
				driver.switchTo().window(parentWindow);
		}
	}
	//handling search 
	@Test(priority = 4)
	public void search() throws IOException {
		WebElement ele = automate_helper.getElementByLocator("id", "search_box_id");
		//automate_helper.getElementByLocator("id", "search_btn_id").click();
		ele.sendKeys("Selinium");
		ele.sendKeys(Keys.ENTER);
	}
	
	@Test(priority =5)
	public void selectElement() {
		
	}
	
	//Close the browser
	@AfterTest
	public void end() {
		driver.close();
	}
}
