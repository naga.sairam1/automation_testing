package support;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.WebElement;


public class automate_helper {

	static WebDriver driver;
	//defining constructor with 1 parameter
	public automate_helper(WebDriver d){
		automate_helper.driver = d;
	}
	//method to get string value from properties file
	// Using Properties library file to import data and locators from .properties file.
	// get the properties file from the main file.
	public static Properties property() throws IOException {
		Properties prop = new Properties();
		String FilePath = System.getProperty("user.dir") + "\\data.properties";
		FileInputStream fs = new FileInputStream(FilePath);
		prop.load(fs);
		return prop;
	}
	
	// writing a method to switch between the available locators.
	//  and we get the locator value from the property file.
	public static WebElement getElementByLocator(String locator, String locator_Value) throws IOException {		
		WebElement ele = null;
		String caseLocator = null;
		     switch(locator.toLowerCase()) 
		     {
		     case "id":
		    	caseLocator = property().getProperty(locator_Value);
		    	ele = driver.findElement(By.id(caseLocator));
		    	break;
		     case "className":
		     caseLocator = property().getProperty(locator_Value);
		    	ele =  driver.findElement(By.className(caseLocator));
		    	 break;
		     case "name":
		    	 caseLocator = property().getProperty(locator_Value);
		    	 ele = driver.findElement(By.name(caseLocator));
		    	 break;
		     case "tagName":
		    	 caseLocator = property().getProperty(locator_Value);
		    	 ele = driver.findElement(By.tagName(caseLocator));
		    	 break;
		     case "linkText":
		    	 caseLocator = property().getProperty(locator_Value);
		    	 ele = driver.findElement(By.linkText(caseLocator));
		    	 break;
		     case "partialLinkText":
		    	 caseLocator = property().getProperty(locator_Value);
		    	 ele = driver.findElement(By.partialLinkText(caseLocator));
		    	 break;
		     case "xpath":
		    	 caseLocator = property().getProperty(locator_Value);
		    	 ele = driver.findElement(By.xpath(caseLocator));
		    	 break;
		     case "cssSelector":
		    	 caseLocator = property().getProperty(locator_Value);
		    	 ele = driver.findElement(By.cssSelector(caseLocator));
		    	 break;
		    default:
		    	System.out.println("give a valid loctor type/locator value");
		     }
		     return ele;
		   
     }
	

}
