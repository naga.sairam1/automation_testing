package ObjectClasses;

import java.io.IOException;

import org.openqa.selenium.WebDriver;

import support.automate_helper;

public class objMainMenu {
	 static WebDriver driver;
	
	//constructor override
	public objMainMenu(WebDriver driver){
		objMainMenu.driver = driver;
	}
	
	public static void menu(int n) throws IOException {
		
		// obj for automate helper class file
		new automate_helper(driver);
		
		// calling method from helper class using object
		// find element using locators : [id, classname, name, tagName, linkText,
		// partialLinkText, xpath, cssSelector]
		for(int i = 1; i<=n ; i++) {
		automate_helper.getElementByLocator("Xpath", "xpath" + i +"").click();
		}
		automate_helper.getElementByLocator("xpath","Logo_xpath").click();
	
	}
	
}
