package ObjectClasses;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class objScreenShot {
	static WebDriver driver;
	
	public objScreenShot(WebDriver driver){
		objScreenShot.driver = driver;
	}
	
	public static void ScreenShot(String ScreenShotName) {
		
		TakesScreenshot screenShot = ((TakesScreenshot)driver);
		File Img1 = screenShot.getScreenshotAs(OutputType.FILE);
		File Img2 = new File("D:\\eclipse-workspace\\eclipse-workspace\\MethodSelenium\\test-output\\testScreenShots\\" +ScreenShotName +".png");
		try {
			FileUtils.copyFile(Img1,Img2);
		}
		catch(IOException e) {
			System.out.println(e.getMessage());
		}
	}
}
