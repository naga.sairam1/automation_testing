package ObjectClasses;

import java.io.IOException;

import org.openqa.selenium.WebDriver;

import support.automate_helper;

public class objLogin {
	static WebDriver driver;

	public objLogin(WebDriver driver) {
		objHandleWindows.driver = driver;
	}
	
	public void userName() throws IOException{
		new automate_helper(driver);
		automate_helper.getElementByLocator("xpath", "user_login_xpath");
	}
	
	public void password() throws IOException {
		new automate_helper(driver);
		automate_helper.getElementByLocator("xpath", "psw_login_xpath");
	}
}
