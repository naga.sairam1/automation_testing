package ObjectClasses;

import java.io.IOException;

import org.openqa.selenium.WebDriver;

import support.automate_helper;

public class objSocialLinks {
	
	static WebDriver driver;
	
	public objSocialLinks(WebDriver d) {
		objSocialLinks.driver = d;
	}
	public static void getSocialLinks(int n) throws IOException {
		new automate_helper(driver);
		
		String parentWindow = driver.getWindowHandle();
		
		for(int i=1;i<n;i++) {
			automate_helper.getElementByLocator("xpath", "social_links_"+ i + "").click();
			driver.switchTo().window(parentWindow);
		}
	}
}
